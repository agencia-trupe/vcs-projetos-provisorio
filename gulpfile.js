var gulp        = require('gulp'),
    stylus      = require('gulp-stylus'),
    jeet        = require('jeet'),
    rupture     = require('rupture'),
    koutoSwiss  = require('kouto-swiss'),
    plumber     = require('gulp-plumber');

gulp.task('stylus', function() {
    return gulp.src(['./assets/stylus/main.styl'])
        .pipe(plumber())
        .pipe(stylus({
            use: [koutoSwiss(), jeet(), rupture()],
            compress: true
        }))
        .pipe(gulp.dest('./assets/css/'))
});

gulp.task('default', function() {
    gulp.watch('./assets/stylus/**/*.styl', ['stylus']);
});
